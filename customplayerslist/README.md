# CustomPlayersList

Replace of `/list` and/or `/players` command with custom window.

### Requirements

- `textdll`
- `raise`

### Configuration

You can change settings in script itself.

- `config_str` string of item
- `config_args` arguments of string item (note: arguments count should be same as arguments of `config_str`)
- `config_label` label settings

### Available arguments

- `"pid"` pid of player
- `"name"` name of player
- `"ping"` ping of player
- `"level"` level of player
- `"cell"` current player cell (this will include outside cells)

### License

CustomPlayersList (c) 2020 
Volk_Milit (aka Ja'Virr-Dar), GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
