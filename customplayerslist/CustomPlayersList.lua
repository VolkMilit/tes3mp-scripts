plstr = "%s (%s) | ping: %s | %s"
plargs = {"name", "pid", "ping", "cell"}
pllabel = {
	one = "Один игрок в сети",
	many = " игрока в сети"
}

local function args_parser(pid)
	local t = {}

	for i, arg in pairs(plargs) do
		if arg == "pid" then
			t[i] = tostring(pid)
		elseif arg == "name" then
			t[i] = Players[pid].data.login.name
		elseif arg == "ping" then
			t[i] = tes3mp.GetAvgPing(pid)
		elseif arg == "level" then
			t[i] = Players[pid].data.stats.level
		elseif arg == "cell" then
			local cell_current = tes3mp.GetCell(pid)
		
			if raise.cells.is_region(cell_current) then
				local region, cell = raise.cells.current_region(pid)
				local region_real = textdll.cel:get(region) or region
				t[i] = string.format("%s (%s)", region_real, cell)
			else
				local cell_real = tes3mp.GetCell(pid)
				t[i] = textdll.cel:get(cell_real) or cell_real
			end
		else
			t[i] = arg
		end
	end
	
	return string.format(plstr, unpack(t))
end

local function populate_list()
	local res = ""
	local last_pid = tes3mp.GetLastPlayerId()

	for ind, player in pairs(Players) do
		if player ~= nil and player:IsLoggedIn() then
			res = res .. args_parser(player.pid) .. "\n"
		end
	end
	
	return res
end

local function show_list(pid)
	local players_count = logicHandler.GetConnectedPlayerCount()
    local label = pllabel.one

    if players_count > 1 then
        label = players_count .. pllabel.many
    end

    tes3mp.ListBox(pid, 2, label, populate_list())
end

customCommandHooks.registerCommand("list", show_list)
customCommandHooks.registerCommand("players", show_list)
