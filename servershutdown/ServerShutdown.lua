local DateTime = require("luastd.datetime")

local local_config = {
	time_shutdown = "12:16",
	announce_periods = {0.5, 1, 2, 3, 4, 5},
	message = "#FF0000[Server]: Server shutting down in %d %s!\n",
	label_hours = "hours",
	label_minutes = "minutes"
}

local mseconds_shutdown = DateTime:mseconds_to(local_config.time_shutdown) - DateTime.minutes_to_msecond(5)

local function anonse(hours)
	local label = label_hours

	if hours * 60 < 60 then
		label = label_minutes
	end

	for pid, player in pairs(Players) do
		if player:IsLoggedIn() then
			local msg = string.format(local_config.message, hours, label)
			player:Message(msg)
		end
	end
end

local function setup_timers()
	local current = DateTime:current_time() * 1000

	for i, k in pairs(local_config.announce_periods) do
		local msec = current - mseconds_shutdown - DateTime.hours_to_msecond(k)
		
		if msec > 0 then
			timerApiEx.CreateTimer("server_shutdown_anonse_" .. tostring(i), msec, function()
				anonse(k)
			end)
			timerApiEx.StartTimer("server_shutdown_anonse_" .. tostring(i))
		end
	end
end

local function timeout()
	local num = timerApiEx.GetCurrentLoopNum("server_shutdown_timeout")
	if num < 4 then
		print(5 - num .. " minutes left to shutdown")
		return
	end
	
	tes3mp.StopServer(0)
end

local function shutdown_sequence()
	local loop_minutes = DateTime.minutes_to_msecond(1)
	timerApiEx.CreateLoopTimer("server_shutdown_timeout", loop_minutes, 5, timeout)
	timerApiEx.StartTimer("server_shutdown_timeout")
end

timerApiEx.CreateTimer("server_shutdown", mseconds_shutdown, shutdown_sequence)
timerApiEx.StartTimer("server_shutdown")

customEventHooks.registerHandler("OnServerPostInit", setup_timers)
