--[[
	Rebirth (tes3mp) - respawn any npc player killed.
    Copyright (C) 2018 Freedom Land Team

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
]]--

json = require("jsonInterface")
logicHandler = require("logicHandler")
tableHelper = require("tableHelper")

Methods = {}
local respawnTime = 1 -- 30 minutes
local respawnAgressiveTime = 90 -- 1.5 hourss

local ignore = {}

-- all agressive npcs generated with help of Construction Set
local agressive = json.load("Rebirth/npc.json")
local creatures = json.load("Rebirth/crea.json")

function Methods.Respawn(pid, cell)
	local timeCurrent = os.time()
	local wantRestore = true
	local isAgressive = false
	local count = 0
	
	-- TODO: check if cell file exist and we have killed npc in there
	if LoadedCells[cell] == nil or WorldInstance.data.killedNpcs[cell] == nil then
		return
	end
	
	local pid = LoadedCells[cell]:GetAuthority()
	
	for id, t in pairs(WorldInstance.data.killedNpcs[cell]) do
		local refId = LoadedCells[cell].data.objectData[id].refId
		
		if refId ~= nil then
			if tableHelper.containsValue(ignore, refId, false) == false then
				-- not creature (must respawn by engine itself)
				if tableHelper.containsValue(creatures, refId, false) == false then
					-- respawn agressive
					if tableHelper.containsValue(agressive, refId, false) == true then
						if timeCurrent - t >= respawnAgressiveTime * 60 then
							logicHandler.RunConsoleCommandOnPlayer(pid, string.format("\"%s\" -> resurrect", refId), false)
							WorldInstance:RemoveKilled(cell, id)
							count = count + 1
						end
					else
						-- respawn everyone else
						if timeCurrent - t >= respawnTime * 60 then
							logicHandler.RunConsoleCommandOnPlayer(pid, string.format("\"%s\" -> resurrect", refId), false)
							WorldInstance:RemoveKilled(cell, id)
							count = count + 1
						end
					end
				end
			end
		end
	end
	
	-- Even if we don't have any NPC killed, we still need to reset actors
	-- pereodically. They have a tendency to leave their places or player
	-- may provoke them leave.
	-- Take only second player, that visit that cell. First player might be
	-- current cell player.
	
	if count > 0 then
		logicHandler.RunConsoleCommandOnPlayer(pid, "ra", false)
	else
		-- There can be more then one visitor in cell, but we only need 
		-- to be sure, that last visit was 60 seconds ago. If there is only
		-- one player visit that particular cell, we should take cell
		-- creation time
		local times = {}
		for name, t in pairs(LoadedCells[cell].data.lastVisit) do
			table.insert(times, t)
		end
		
		if #times > 1 then
			if timeCurrent - times[2] >= 60 then
				logicHandler.RunConsoleCommandOnPlayer(pid, "ra", false)
			end
		else
			if timeCurrent - LoadedCells[cell].data.entry.creationTime >= 60 then
				logicHandler.RunConsoleCommandOnPlayer(pid, "ra", false)
			end
		end
	end
end

function Methods.ForceRespawn(pid)
	local cell = tes3mp.GetCell(pid)
	local count = 0

	if LoadedCells[cell] == nil then
		Players[pid]:Message("// #FF0000Ячейка не загружена.\n")
		return
	end
	
	if WorldInstance.data.killedNpcs[cell] == nil then
		Players[pid]:Message("// #FF0000В ячейке нет убитых NPC.\n")
		return
	end
	
	local pid = LoadedCells[cell]:GetAuthority()
	
	for id, t in pairs(WorldInstance.data.killedNpcs[cell]) do
		local refId = LoadedCells[cell].data.objectData[id].refId
		logicHandler.RunConsoleCommandOnPlayer(pid, string.format("\"%s\" -> resurrect", refId), false)
		count = count + 1
	end
	
	if count > 0 then
		logicHandler.RunConsoleCommandOnPlayer(pid, "ra", false)
		WorldInstance.data.killedNpcs[cell] = nil
	end
end

function Methods.CellInfo(pid)
	local cell = tes3mp.GetCell(pid)
	
	if LoadedCells[cell] == nil then
		Players[pid]:Message("//#FF0000Ячейка не загружена.\n")
		return
	end
	
	local tmp = {}
	tmp = jsonInterface.load("cell/" .. cell .. ".json")
	
	local message = "File:\n"
	
	for i, t in pairs(tmp.packets.actorList) do
		message = message .. t .. "\n"
	end
	
	message = message .. "\nNPCs\n"
	
	for i, t in pairs(tmp.packets.actorList) do
		local obj = tmp.objectData[t]
		if obj ~= nil then
			message = message .. obj.refId .. "(" .. t .. "): " .. obj.stats.healthCurrent .. "\n"
		else
			message = message .. t .. ": nil\n"
		end
	end
	
	tmp = nil
	
	message = message .. "Memory:\n\nStats dynamic:\n"
	
	for i, t in pairs(LoadedCells[cell].data.packets.actorList) do
		message = message .. t .. "\n"
	end
	
	message = message .. "\nNPCs\n"
	
	for i, t in pairs(LoadedCells[cell].data.packets.actorList) do
		local obj = LoadedCells[cell].data.objectData[t]
		if obj ~= nil then
			message = message .. obj.refId .. "(" .. t .. "): " .. obj.stats.healthCurrent .. "\n"
		else
			message = message .. t .. ": nil\n"
		end
	end
	
	tes3mp.CustomMessageBox(pid, -1, message, "Ok")
end

return Methods
