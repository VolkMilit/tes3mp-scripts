# Rebirth

Rebirth (tes3mp 0.7) - respawn any npc player killed.

### How to install
Put Rebirth directory in your TES3MP installation CoreScripts/data, put Rebirth.lua in CoreScripts/scripts. In server.lua put rebirth = require("Rebirth") in top of serverCore.lua

Found OnCellLoad(pid, cellDescription) and put rebirth.Respawn(cellDescription) right after myMod.OnCellUnload(pid, cellDescription) .

You can change respawn time in Rebirth.lua: respawnTime = 30 -- 30 minutes

### Note

This plugin requires that you separate global kills to per-player basis.

Plugin is NOT ideal. I can't help it. Sometimes NPCs doesn't respawn at all. Also NPCs with same ID will never respawn correctly.

### License
Copyright (C) 2018-2019 Volk_Milit (aka Ja'Virr-Dar), GPL v3.0
