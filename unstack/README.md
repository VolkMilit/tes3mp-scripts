# UnStack

Replace for fixme command. Works better.

### How does it works?

It actually doing same job, as original fixme in CoreScripts, but with one change: first, player Z coordinate changed to 1000 and then apply fixme. This looks like ducttape, but actually works pretty well.

If player is indoors, coc <cell description> is triggered instead of (buggy) fixme.

This file will **override** original `/fixme` from chat. And it supported `config.fixmeInterval`.

### How to install

- Put timerApiEx in CoreScripts/scripts
- Add `require("unstack")` to `customScripts.lua`

### License

UnStack (c) 2020 Volk_Milit (aka Ja'Virr-Dar), GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
