timer = nil

function fixmetimer(pid)
	logicHandler.RunConsoleCommandOnPlayer(pid, "fixme", false)
	
	tes3mp.StopTimer(timer)
	--tes3mp.FreeTimer(timer)
end

local function fixme(pid)
	local currentTime = os.time()
	local cell = tes3mp.GetCell(pid)

	if not LoadedCells[cell].isExterior then
		tes3mp.MessageBox(pid, -1, "You can't use /fixme here.")
		return
	end

	if Players[pid].lastFixMe == nil or
		currentTime >= Players[pid].lastFixMe + config.fixmeInterval then
		
		local X = tes3mp.GetPosX(pid)
		local Y = tes3mp.GetPosY(pid)
		local Z = 1000
			
		tes3mp.SetPos(pid, X, Y, Z)
		tes3mp.SendPos(pid)
			
		timer = tes3mp.CreateTimerEx("fixmetimer", time.seconds(1), "i", pid)
		tes3mp.StartTimer(timer)
		
		Players[pid].lastFixMe = currentTime
		tes3mp.MessageBox(pid, -1, "Looks like you're get of this place.")
	else
		local remainingSeconds = Players[pid].lastFixMe + config.fixmeInterval - currentTime
		local s = 's'
		
		if remainingSeconds <= 1 then
			remainingSeconds = "one"
			s = ''
		end

		local message = string.format("You can use /fixme again in %s second%s.", remainingSeconds, s)
		tes3mp.MessageBox(pid, -1, message)
	end
end

customCommandHooks.registerCommand("fixme", fixme)
