# TimerApiEx

Wrapper for timers in tes3mp, to easy create and control dozen of timers.

### How to install

- Put timerApiEx in `server/scripts/custom`
- In `customScripts.lua` add:

```lua
timerApiEx = require("timerApiEx")
```

### How to use

```lua
-- create regular timer
timerApiEx.CreateTimer("test1", time.seconds(1), function() Players[pid]:Message("Hello from regular timer!\n") end)
timerApiEx.StartTimer("test1")

-- create loop timer
timerApiEx.CreateTimer("test2", time.seconds(1), 5, function() Players[pid]:Message("Hello from loop timer!\n") end)
timerApiEx.StartTimer("test2")

-- get iterations from loop timer
timerApiEx.CreateTimer("test3", time.seconds(1), 5, function() Players[pid]:Message("Hello from loop timer, iteration " .. timerApiEx.GetCurrentLoopNum("test3") .. "!\n") end)
timerApiEx.StartTimer("test3")

-- create ~~flood~~ infinitive loop timer
timerApiEx.CreateTimer("test4", time.seconds(1), -1, function() Players[pid]:Message("Hello from infinitive loop timer!\n") end)
timerApiEx.StartTimer("test4")
```

### Note

All timers is self-destructed, after work is done, timer will destroy itself.

### License
TimerApiEx (c) 2019-2020 
Freedom Land Team / Volk_Milit (aka Ja'Virr-Dar), GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
