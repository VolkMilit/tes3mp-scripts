local Methods = {}
local thisdata = {}

function Call(id)
	if id == nil then
		tes3mp.LogMessage(enumerations.log.ERROR, "Timer id is nil!")
		return
	end

	if thisdata[id] == nil then
		tes3mp.LogMessage(enumerations.log.ERROR, string("Timer (%s) table is nil!").format(id))
		return
	end
	
	if thisdata[id].func == nil then
		tes3mp.LogMessage(enumerations.log.ERROR, string("Timer (%s) func is nil!").format(id))
		return
	end

	thisdata[id].func()
	
	if thisdata[id].loop ~= nil then
	
		if thisdata[id].loop == -1 then
			tes3mp.RestartTimer(thisdata[id].timer, thisdata[id].t)
			return
		end
	
		thisdata[id].loopnum = thisdata[id].loopnum + 1
	
		if thisdata[id].loop == thisdata[id].loopnum then
			tes3mp.StopTimer(thisdata[id].timer)
			thisdata[id] = nil
		else
			tes3mp.RestartTimer(thisdata[id].timer, thisdata[id].t)
		end
	
	else
		--tes3mp.StopTimer(thisdata[id].timer)
		--tes3mp.FreeTimer(thisdata[id].timer)
		thisdata[id] = nil
	end
end

function Methods.GetCurrentLoopNum(id)
	return thisdata[id].loopnum
end

function Methods.StartTimer(id)
	tes3mp.StartTimer(thisdata[id].timer)
end

function Methods.StopTimer(id)
	if thisdata[id] ~= nil then
		tes3mp.StopTimer(thisdata[id].timer)
		tes3mp.FreeTimer(thisdata[id].timer)
		thisdata[id] = nil
	end
end

function Methods.CreateTimer(id, t, func)
	assert(type(id) == "string", "id must be string, got " .. type(id))
	assert(type(t) == "number", "t must be number, got " .. type(id))
	assert(type(func) == "function", "func must be function, got " .. type(func))

	thisdata[id] = {}
	thisdata[id].timer = tes3mp.CreateTimerEx("Call", t, "s", id)
	thisdata[id].func = func
end

function Methods.CreateLoopTimer(id, t, loop, func)
	assert(type(id) == "string", "id must be string, got " .. type(id))
	assert(type(t) == "number", "t must be number, got " .. type(id))
	assert(type(loop) == "number", "loop must be number, got " .. type(loop))
	assert(type(func) == "function", "func must be function, got " .. type(func))

	thisdata[id] = {}
	thisdata[id].timer = tes3mp.CreateTimerEx("Call", t, "s", id)
	thisdata[id].t = t
	thisdata[id].func = func
	thisdata[id].loop = loop
	thisdata[id].loopnum = 0
end

return Methods
