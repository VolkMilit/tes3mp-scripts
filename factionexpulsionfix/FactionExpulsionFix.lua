local function OnPlayerFaction(eventStatus, pid, action)
    if Players[pid] ~= nil and Players[pid]:IsLoggedIn() then             
		if action == enumerations.faction.RANK then
			Players[pid]:SaveFactionRanks()
		elseif action == enumerations.faction.REPUTATION then                
			Players[pid]:SaveFactionReputation()
		--elseif action == enumerations.faction.EXPULSION then	
		--	tes3mp.MessageBox(pid, -1, "Faction uses EXPULSION! It was not very effective...")
		end
    end
    
    return customEventHooks.makeEventStatus(false,true)
end

customEventHooks.registerValidator("OnPlayerFaction", OnPlayerFaction)
