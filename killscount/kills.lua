local function save_kills_player(eventStatus, pid)
	if eventStatus.validCustomHandlers then
        local player = Players[pid]
        
		if player.data.kills == nil then
			player.data.kills = {}
		end
		
        local refId = ""
        local count = 0
        
		for index = 0, tes3mp.GetKillChangesSize(pid) - 1 do
			refId = tes3mp.GetKillRefId(pid, index)
			
			count = player.data.kills[refId]
					
			if count then
				Players[pid].data.kills[refId] = count + 1
			else
				Players[pid].data.kills[refId] = 1
                count = 1
			end
		end
        
        customEventHooks.triggerHandlers("OnActorKilled", customEventHooks.makeEventStatus(true, true), 
                                         {pid, tes3mp.GetCell(pid), refId, count})
					
		Players[pid]:QuicksaveToDrive()
		
		return customEventHooks.makeEventStatus(false,true)
	end
end

local function load_player_kills(eventStatus, pid)
	if eventStatus.validCustomHandlers then	
		if Players[pid].data.kills == nil then
			return
		end

		tes3mp.ClearKillChanges(pid)
		
		for refId, number in pairs(Players[pid].data.kills) do
			if refId ~= nil and refId ~= ""  then
				tes3mp.AddKill(pid, refId, number)
			end
		end
		
		tes3mp.SendKillChanges(pid)
	end
end

customEventHooks.registerHandler("OnPlayerFinishLogin", load_player_kills)
customEventHooks.registerValidator("OnWorldKillCount", save_kills_player)
