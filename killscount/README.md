# Kills Count

Separate kills per-player basis.

### Custom handler

As a bonus, there is custom handler with pid, cell, refId and count. To use it you must do something like that:

```lua
    local function on_creature_death(eventStatus, pid, cellDescription, refId, count)
        if eventStatus.validCustomHandlers then
            Players[pid]:Message(cellDescription .. "\n" .. refId .. "\n" .. count .. "\n")
        end
    end
    
    customEventHooks.registerHandler("OnActorKilled", on_creature_death)
```

### License

Kills Count (c) 2020 
Volk_Milit (aka Ja'Virr-Dar), GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
