local function MessageBox(pid)
    local id = mpWidgets.New("Test ListWidget")
    mpWidgets.AddButton(id, "Button 1", function(pid) mpWidgets.Notification(pid, "List 1 selected!") end)
    mpWidgets.AddButton(id, "Button 2", function(pid) mpWidgets.Notification(pid, "List 2 selected!") end)
    mpWidgets.AddButton(id, "Button 3", function(pid) mpWidgets.Notification(pid, "List 3 selected!") end)
    mpWidgets.AddButton(id, "Button 4", function(pid) mpWidgets.Notification(pid, "List 4 selected!") end)
    mpWidgets.AddButton(id, "Close", nil)
    mpWidgets.DeleteLater(id)
    mpWidgets.Show(pid, id, mpWidgets.Type.MessageBox)
end

local function ListWidget(pid)
    local id = mpWidgets.New("Test ListWidget")
    mpWidgets.AddButton(id, "* Close *", nil)
    mpWidgets.AddButton(id, "List 1", function(pid) mpWidgets.Notification(pid, "List 1 selected!") end)
    mpWidgets.AddButton(id, "List 2", function(pid) mpWidgets.Notification(pid, "List 2 selected!") end)
    mpWidgets.AddButton(id, "List 3", function(pid) mpWidgets.Notification(pid, "List 3 selected!") end)
    mpWidgets.AddButton(id, "List 4", function(pid) mpWidgets.Notification(pid, "List 4 selected!") end)
    mpWidgets.DeleteLater(id)
    mpWidgets.Show(pid, id, mpWidgets.Type.ListWidget)
end

local function CheckList(pid)
    local id = mpWidgets.New("Test CheckList")
    mpWidgets.AddCheckBox(id, "Check 1", false)
    mpWidgets.AddCheckBox(id, "Check 2", false)
    mpWidgets.AddCheckBox(id, "Check 3", false)
    mpWidgets.OnOkClicked(id, function(pid, tab)
        Players[pid]:Message(pid .. ": " .. tableHelper.getSimplePrintableTable(tab))
    end)
    mpWidgets.DeleteLater(id)
    mpWidgets.Show(pid, id, mpWidgets.Type.CheckList)
end

local function Counter(pid)
    local id = mpWidgets.New("Test Counter")    
    mpWidgets.AddCounter(id, 
                        function(num)
                            mpWidgets.Notification(pid, "Number is " .. num)
                        end, 
                        0, false)
    mpWidgets.SetLimits(id, 0, 100)
    mpWidgets.DeleteLater(id)
    mpWidgets.Show(pid, id, mpWidgets.Type.Counter)
end

local function SMessageBox(pid)
    mpWidgets.MessageBox(pid, "Just test MessageBox. Press OK to exit.")
end

local function Notification(pid)
    mpWidgets.Notification(pid, "Just test notification. Nothing to see here.")
end

local function test(pid)
    local id = mpWidgets.New("Widget test")
    mpWidgets.AddButton(id, "Test MessageBox with buttons", MessageBox)
    mpWidgets.AddButton(id, "Test ListWidget", ListWidget)
    mpWidgets.AddButton(id, "Test CheckList", CheckList)
    mpWidgets.AddButton(id, "Test Counter", Counter)
    mpWidgets.AddButton(id, "Test MessageBox with Ok button", SMessageBox)
    mpWidgets.AddButton(id, "Test Notification", Notification)
    mpWidgets.DeleteLater(id)
    mpWidgets.Show(pid, id, mpWidgets.Type.MessageBox)
end

customCommandHooks.registerCommand("testwidgets", test)
