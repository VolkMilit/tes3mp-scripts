# mpWidgets

Dead simple wrapper for tes3mp's CustomMessageBox, ListBox and other boxes.

Note: it's actually a hack, tes3mp doesn't allow server update window (yet), so we recreated window every time for some widgets.

### How to install

1. Put mpWidgets.lua to server/custom
2. Add to customScripts.lua someplace:

```lua
mpWidgets = require("mpWidgets")
```

### How to use

See `testWidgets.lua`

### License

mpWidgets (c) 2019-2020
Freedom Land Team / Volk_Milit (aka Ja'Virr-Dar), GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

