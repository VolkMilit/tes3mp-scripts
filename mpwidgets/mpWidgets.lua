local Methods = {
	Type = tableHelper.enum{"MessageBox", "ListWidget", "TextEdit", "CheckList", "Counter"}
}

local thisdata = {}
local current_idnum = 9999

local function BuildChecklist(pid, id)
	local buttons = ""

	for idx, button in pairs(thisdata[id].buttons) do
        -- Similar to C's allow_all ? "[x]" : "[ ]"
		local check_state = button.checked and "[x] " or "[ ] "
        buttons = buttons .. check_state .. button.name .. ";"
	end
		
	buttons = buttons .. "Done" .. ";"
	
	return buttons
end

local function generate_id()
	if current_idnum >= 2147483646 then
        current_idnum = 9999
    end
    
    current_idnum = current_idnum + 1
	local id = current_idnum
	
	if thisdata[id] ~= nil then
		return generate_id()
	end
	
	return id
end

function Methods.MessageBox(pid, message)
	assert(type(pid) == "number", "pid must be number, got " .. type(pid))
	assert(type(message) == "string", "message must be string, got " .. type(message))
	tes3mp.CustomMessageBox(pid, -1, message, "Ok")
end

function Methods.Notification(pid, message)
	assert(type(pid) == "number", "pid must be number, got " .. type(pid))
	assert(type(message) == "string", "message must be string, got " .. type(message))
	tes3mp.MessageBox(pid, -1, message)
end

function Methods.New(message, title)
	local id = generate_id()
	
	thisdata[id] = {}
	thisdata[id].buttons = {}
	
	if message ~= nil then
		thisdata[id].message = message
	else
		thisdata[id].message = ""
	end
	
	if title ~= nil then
		thisdata[id].title = title
	else
		thisdata[id].title = ""
	end
	
	return id
end

function Methods.SetLimits(id, lmin, lmax)
	thisdata[id].lmin = lmin
	thisdata[id].lmax = lmax
end

function Methods.AddCounter(id, func, startcount, accuracy)
    if thisdata[id] == nil then
        tes3mp.LogAppend(enumerations.log.ERROR, "- mpWidgets: counter created without New.")
        return
    end
    
	thisdata[id].buttons[0] = {}
	thisdata[id].buttons[0].name = "-"
	thisdata[id].buttons[0].func = func
	
	thisdata[id].buttons[1] = {}
	thisdata[id].buttons[1].name = "+"
	thisdata[id].buttons[1].func = func
    
    thisdata[id].buttons[2] = {}
	thisdata[id].buttons[2].name = "Done"
	
	if accuracy == nil then
		thisdata[id].accuracy = false
	else
		thisdata[id].accuracy = accuracy
	end
	
	if startcount == nil then
		thisdata[id].count = 0
	else
		thisdata[id].count = startcount
	end
	
	thisdata[id].message = thisdata[id].title .. "\n" .. thisdata[id].count
end

function Methods.AddCheckBox(id, name, checked, func)    
	if thisdata[id] == nil then
		thisdata[id] = {}
		thisdata[id].buttons = {}
	end
	
	local num = tableHelper.getCount(thisdata[id].buttons)
	
	thisdata[id].buttons[num] = {}
	thisdata[id].buttons[num].name = name
	
	if checked ~= nil then
		thisdata[id].buttons[num].checked = checked
	else
		thisdata[id].buttons[num].checked = false
	end
    
    if func ~= nil then
        thisdata[id].buttons[num].func = func
    end
end

function Methods.OnOkClicked(id, func)
	thisdata[id].func = func
end

function Methods.OnDefaultAction(id, func)
    thisdata[id].default_action = func
end

-- Depricated
function Methods.OnDoneClicked(pid, id, func)
	Methods.OnOkClicked(id, func)
end

function Methods.AddButton(id, name, func, disable_if)
	local num = tableHelper.getCount(thisdata[id].buttons)
    thisdata[id].buttons[num] = {}
    
    if disable_if then
        name = "#b3a887" .. name
        thisdata[id].buttons[num].disabled = true
    end

	thisdata[id].buttons[num].func = func
	thisdata[id].buttons[num].name = name
end

function Methods.GetUserdata(id, num)
	return thisdata[id].buttons[num].userdata
end

function Methods.Show(pid, id, widgetType)
	assert(type(pid) == "number", "pid must be number, got " .. type(pid))
	assert(type(id) == "number", "id must be number, got " .. type(id))

	local buttons = ""
	local divider = ""
	
	thisdata[id].widgetType = widgetType
	
	if widgetType == Methods.Type.MessageBox then
		divider = ";"
	elseif widgetType == Methods.Type.ListWidget then
		divider = "\n"
	else
		divider = ";"
	end
	
	if widgetType == Methods.Type.MessageBox or 
	widgetType == Methods.Type.ListWidget or 
	widgetType == Methods.Type.Counter then
		for idx, button in pairs(thisdata[id].buttons) do
            if button.checked ~= nil then
                -- Similar to C's allow_all ? "[x]" : "[ ]"
                local check_state = button.checked and "[x] " or "[ ] "
                buttons = buttons .. check_state .. button.name .. divider
            else
                buttons = buttons .. button.name .. divider
            end
		end
	elseif widgetType == Methods.Type.CheckList then
		buttons = BuildChecklist(pid, id)
	end
	
	if widgetType == Methods.Type.MessageBox then
		tes3mp.CustomMessageBox(pid, id, thisdata[id].message, buttons)
	elseif widgetType == Methods.Type.ListWidget then
		tes3mp.ListBox(pid, id, thisdata[id].message, buttons)
	elseif widgetType == Methods.Type.TextEdit then
		tes3mp.InputDialog(pid, id, thisdata[id].message, thisdata[id].title)
	else
		tes3mp.CustomMessageBox(pid, id, thisdata[id].message, buttons)
	end
end

function Methods.Delete(id)
    thisdata[id] = nil
end

function Methods.DeleteLater(id)
    thisdata[id].delete_later = true
end

function Methods.OnGUIAction(eventStatus, pid, idGui, data)
	if eventStatus.validCustomHandlers then
		if thisdata[idGui] ~= nil then
			
            if thisdata[idGui].widgetType == Methods.Type.ListWidget and thisdata[idGui].buttons[num] == nil and thisdata[idGui].default_action ~= nil then
                thisdata[idGui].default_action(pid)
            end
            
			if thisdata[idGui].widgetType == Methods.Type.MessageBox or 
			thisdata[idGui].widgetType == Methods.Type.ListWidget then
				local num = tonumber(data)
                local button = thisdata[idGui].buttons[num]
                
                if button ~= nil and button.disabled then
                    Methods.Show(pid, idGui, thisdata[idGui].widgetType)
                    return
                end
                
                -- OR
                if button ~= nil and button.checked ~= nil then
                    button.checked = not button.checked
                end

				if button ~= nil and button.func ~= nil then
					button.func(pid)
                elseif thisdata[idGui].delete_later then
                    thisdata[idGui] = nil
				end
			elseif thisdata[idGui].widgetType == Methods.Type.CheckList then
				local num = tonumber(data)
                local button = thisdata[idGui].buttons[num]
                local lmin = thisdata[idGui].lmin
                local lmax = thisdata[idGui].lmax
                local message = thisdata[idGui].message
                
				if button == nil then
					local t = {}
					
					for idx, btn in pairs(thisdata[idGui].buttons) do
						table.insert(t, btn.checked)
					end
					
					if lmin ~= nil then
						local needReload = false
					
						if #t < lmin then
							tes3mp.MessageBox(pid, -1, "#FF0000Not enouth options checked. Minimum is " .. lmin)
							needReload = true
						end
					
						if #t > lmax then
							tes3mp.MessageBox(pid, -1, "#FF0000You reach limit in options. Maximum is " .. lmax)
							needReload = true
						end
						
						if needReload then
							tes3mp.CustomMessageBox(pid, idGui, message, BuildChecklist(pid, idGui))
						end
					end
								
					thisdata[idGui].func(pid, t)
                    if thisdata[idGui].delete_later then
                        thisdata[idGui] = nil
                    end
				else
					button.checked = not button.checked
					tes3mp.CustomMessageBox(pid, idGui, message, BuildChecklist(pid, idGui))
				end
			elseif thisdata[idGui].widgetType == Methods.Type.Counter then
				local num = tonumber(data)
				local a = 1
				local isValid = true
				
				if thisdata[idGui].accuracy == true then
					a = 0.01
				end
				
				if num == 0 then -- plus clicked
					thisdata[idGui].count = thisdata[idGui].count + a
				elseif num == 1 then -- minus clicked
					thisdata[idGui].count = thisdata[idGui].count - a
				elseif num == 2 then -- done clicked
                    -- Only execute when OnOkClicked is set
					if thisdata[idGui].func ~= nil then
						thisdata[idGui].func(thisdata[idGui].count)
					end
                    
                    if thisdata[idGui].delete_later then
                        thisdata[idGui] = nil
                    end
                    
                    return
				end		
				
				if thisdata[idGui].count < thisdata[idGui].lmin then
					tes3mp.MessageBox(pid, -1, "#FF0000You reach the minimum.")
					thisdata[idGui].count = thisdata[idGui].count + a
					isValid = false
				end
					
				if thisdata[idGui].count > thisdata[idGui].lmax then
					tes3mp.MessageBox(pid, -1, "#FF0000You reach the maximum.")
					thisdata[idGui].count = thisdata[idGui].count - a
					isValid = false
				end
				
				if isValid == true then
					thisdata[idGui].buttons[num].func(thisdata[idGui].count)
				end
				
				local buttons = ""
				for idx, button in pairs(thisdata[idGui].buttons) do
					buttons = buttons .. button.name .. ";"
				end
				tes3mp.CustomMessageBox(pid, idGui, thisdata[idGui].title .. "\n" .. thisdata[idGui].count, buttons)
			elseif thisdata[idGui].widgetType == Methods.Type.TextEdit then
				if thisdata[idGui].func ~= nil then
					thisdata[idGui].func(pid, tostring(data))
                    
                    if thisdata[idGui].delete_later then
                        thisdata[idGui] = nil
                    end
				end
			end
		end
	end
end

customEventHooks.registerHandler("OnGUIAction", Methods.OnGUIAction)

return Methods
