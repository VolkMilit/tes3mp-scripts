json = require("jsonInterface")
require("libluastd")
timerApiEx = require("timerApiEx")

Methods = {}

local timer = 120
local name = "Майкл"
local prefix = "-Не'Лжец"
local quote_storage = json.load("Messages/storage.json")
local msg_num = 1

function message()
	if std.table_length(Players) > 1 then
		local amsg = quote_storage.anonses[msg_num]
		local msg = string.format("#FF00BA%s%s: #FFFFFF%s\n", name, prefix, amsg)
		
		for pid,_ in pairs(Players) do
			Players[pid]:Message(msg)
		end
			
		msg_num = msg_num + 1

		if msg_num > #messages then
			msg_num = 1
		end
	end
end

timerApiEx.CreateTimer("bot_anons", time.seconds(timer), -1, message)
timerApiEx.StartTimer("bot_anons")

local function find_answer(pid, str)
	local find = nil

	for i, t in pairs(quoteStorage) do
		if str:match(i) then
			find = getRandValue(t)
			
			find = find:gsub("%$name", name)
			find = find:gsub("%$pcname", Players[pid].data.login.name)
			find = find:gsub("%$pcrace", Players[pid].data.character.race)
			break
		end
	end
	
	return find
end

function commands(pid, cmd)
	if cmd:match(name .. ",+%s*") then
				
		local st = cmd:split(",")[2]
		local msg = ""
	
		if st == nil then
			msg = string.format("#FF00BA %s%s: #FFFFFF%s знает множество вещей. Почему исчезли двемеры? Этого %s не знает.\n",  name, prefix, name, name)
			Players[pid]:Message(msg)
		else
			local find = FindAns(pid, st)
			if find ~= nil then
				msg = string.format("#FF00BA%s%s: #FFFFFF%s\n", name, prefix, find)
				Players[pid]:Message(msg)
			else
				msg = string.fromat("#FF00BA%s%s: #FFFFFF%s знает лишь те слова, которые ему дали при рождении. Таких слов %s не знает.\n", name, prefix, name, name)
				Players[pid]:Message(msg)
			end
		end
		
	end
end
