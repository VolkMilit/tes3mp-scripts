# Anonser Bot

Mai'q but in chat.

### Requirements:

- tes3mp 0.7
- timerApiEx
- [luastd](https://gitlab.com/VolkMilit/luastd)

### How to install

1. Put AnonserBot.lua to CoreScripts/scripts
2. Put Messages directory to CoreScripts/data
3. In customScripts.lua add in very top:

```lua
require("AnonserBot")
```

### License

Anonser Bot (c) 2019 Freedom Land Team, GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

