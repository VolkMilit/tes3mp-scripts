# Textdll

Read cel data files.

Helper class to read `cel` and `top` data files, useful only for Russian servers, as `cel` and `top` files introduced only in 1C version.

`cel` directory contains `regions.cel` file, which is holds translation for regions.

### Installation

- Copy textdll.lua to `ServerScripts/scripts/custom`
- Add to customScripts.lua:

```lua
textdll = require("textdll")
```

- Copy morrowind.cel, tribunal.cel, bloodmoon.cel to data/custom/cel, if you will

### Usage

```
-- Will read all Morrowind cells data
local celdata = textdll.cel({tes3mp.GetDataPath() .. "/cel/morrowind.cel"})
print(celdata:get("Balmora")) -- Will print 'Балмора' on 1C version

-- Will read all Morrowind top data
local topdata = textdll.top({tes3mp.GetDataPath() .. "/top/morrowind.top"})
print(celdata:get("alit")) -- Will print 'алит' on 1C version
```

### Note

All files must be ASCII (Windows-1251) formated, or textdll will be unable to read it. Library will handle and print errors and your server will not throw exception if file failed to read.

### License

Textdll (c) 2020 
Volk_Milit (aka Ja'Virr-Dar), GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
