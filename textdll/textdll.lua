local data_path = tes3mp.GetDataPath()
local files = {
    cel = {
        data_path .. "/custom/cel/morrowind.cel",
        data_path .. "/custom/cel/tribunal.cel",
        data_path .. "/custom/cel/bloodmoon.cel",
        data_path .. "/custom/cel/regions.cel"
    },
    
    top = {}
}

local encoding = {
	"А", "Б", "В", "Г", "Д", "Е", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я", "а", "б", "в", "г", "д", "е", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я"
}

local function ascii_to_cyrillic(str)
	assert(type(str) == "string", "str string expected, got " .. type(str))

    local result = {}
    
    for i = 1, string.len(str) do
		local byte = string.byte(str, i)
		
		if byte == 32 then
			table.insert(result, " ")
		elseif byte >= 191 then
			table.insert(result, encoding[(191-byte)*-1] or "?")
		end
    end
    
    return table.concat(result)
end

-- Since I want to share this script, I don't use my borked luastd here
local function file_exist(file)
    local f=io.open(file, "r")
    if f~=nil then io.close(f) return true else return false end
end

local function is_cyrillic(str)
	-- Use second byte here, because 
	-- some strings contains space or ' on first byte
	local second_byte = string.byte(str, 2)
	return second_byte >= 191
end

local textdll = {
	cel = {
		data = {}
	},
	
	top = {
		data = {}
	}
}

function textdll.load()
    for i, k in pairs(files.cel) do
        if file_exist(k) then
            textdll.cel:parse_cel(k)
        else
            print(k .. ": not such file or directory")
        end
    end
    
    for i, k in pairs(files.top) do
        if file_exist(k) then
            textdll.top:parse_top(k)
        else
            print(k .. ": not such file or directory")
        end
    end
end

function textdll.cel:parse_cel(path)
	local i = 1

	for line in io.lines(path) do
		i = i + 1
		local key, value = unpack(line:split("\t"))
		if key and value then
			self.data[key] = ascii_to_cyrillic(value)
		else
			print("[textdll] Wrong string on line " .. i)
		end
	end
end

function textdll.top:parse_top(path)
	for line in io.lines(tes3mp.GetDataPath() .. "/" .. path) do
		local key, value = unpack(line:split("\t"))
		
		if is_cyrillic(key) then
			key = ascii_to_cyrillic(key)
		end
		
		self.data[key] = ascii_to_cyrillic(value)
	end
end

function textdll.cel:get(cell_description)
	return self.data[cell_description]
end

function textdll.top:get(topic)
	return self.data[topic]
end

customEventHooks.registerHandler("OnServerPostInit", textdll.load)

return textdll
