local function save_admin_attributes(pid)
    for name in pairs(Players[pid].data.attributes) do
        local attributeId = tes3mp.GetAttributeId(name)

        local baseValue = tes3mp.GetAttributeBase(pid, attributeId)
		
		Players[pid].data.attributes[name] = {
			base = baseValue,
			damage = tes3mp.GetAttributeDamage(pid, attributeId),
			skillIncrease = tes3mp.GetSkillIncrease(pid, attributeId)
		}
	end
end

local function save_admin_skills(pid)
    for name in pairs(Players[pid].data.skills) do

        local skillId = tes3mp.GetSkillId(name)

        local baseValue = tes3mp.GetSkillBase(pid, skillId)

        Players[pid].data.skills[name] = {
			base = baseValue,
			damage = tes3mp.GetSkillDamage(pid, skillId),
			progress = tes3mp.GetSkillProgress(pid, skillId)
		}
	end
end

local function superman(pid, cmd)
	if Players[pid].data.settings.staffRank < 2 then
		return
	end

	if cmd[2] == "on" then
		Players[pid].custom_skills = {}
		Players[pid].custom_skills["Speed"] = Players[pid].data.attributes["Speed"].base
		Players[pid].custom_skills["Acrobatics"] = Players[pid].data.skills["Acrobatics"].base
		
		Players[pid].data.attributes["Speed"].base = 1000
		Players[pid].data.skills["Acrobatics"].base = 1000
		Players[pid]:LoadSkills()
		Players[pid]:LoadAttributes()
		
		logicHandler.RunConsoleCommandOnPlayer(pid, "tgm", false)
		logicHandler.RunConsoleCommandOnPlayer(pid, "tcl", false)
		--save_admin_attributes(pid)
		--save_admin_skills(pid)
		tes3mp.MessageBox(pid, -1, [[
			#C27D00
			Superman activated!
			Acrobatics set to 1000!
			Speed set to 1000!
			tgm and tcl activated!
		]])
	elseif cmd[2] == "off" then
		if Players[pid].custom_skills == nil then
			Players[pid]:Message("/superman was not activated, you should activate it first.\n")
		end
	
		Players[pid].data.attributes["Speed"].base = Players[pid].custom_skills["Speed"]
		Players[pid].data.skills["Acrobatics"].base = Players[pid].custom_skills["Acrobatics"]

		Players[pid]:LoadSkills()
		Players[pid]:LoadAttributes()

		logicHandler.RunConsoleCommandOnPlayer(pid, "tgm", false)
		logicHandler.RunConsoleCommandOnPlayer(pid, "tcl", false)
		--save_admin_attributes(pid)
		--save_admin_skills(pid)
		
		Players[pid].custom_skills = nil
		
		tes3mp.MessageBox(pid, -1, "#C27D00Superman desactivated!")
	else
		Players[pid]:Message("/superman: use on or off as argument.\n")
	end
end

customEventHooks.registerValidator("OnPlayerAttribute", function(eventStatus, pid)
    if Players[pid].data.settings.staffRank >= 2 then
		save_admin_attributes(pid)
        return customEventHooks.makeEventStatus(false,true)
	else
		Players[pid]:SaveAttributes()
    end
end)

customEventHooks.registerValidator("OnPlayerSkill", function(eventStatus, pid)
    if Players[pid].data.settings.staffRank >= 2 then
		save_admin_skills(pid)
        return customEventHooks.makeEventStatus(false,true)
    else
		Players[pid]:SaveSkills()
    end
end)

customCommandHooks.registerCommand("superman", superman)
