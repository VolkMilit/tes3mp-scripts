return {
	actors = require("custom/raise.actors"),
	classes = require("custom/raise.classes"),
	console = require("custom/raise.console"),
	diseases = require("custom/raise.diseases"),
	inventory = require("custom/raise.inventory"),
	player = require("custom/raise.player"),
	server = require("custom/raise.server"),
	sfx = require("custom/raise.sfx"),
	world = require("custom/raise.world"),
	cells = require("custom/raise.cells"),
	records = require("custom/raise.records"),
	objects = require("custom/raise.objects"),
    time = require("custom/raise.time"),
    menus = require("custom/raise.menus"),
    items = require("custom/raise.items")
}
