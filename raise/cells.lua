local cells = {}

local data = {["0, 10"]="Vemynal",["0, 14"]="Kogoruhn",["0, 22"]="Vas",["0, -7"]="Pelagiad",["0, -8"]="Pelagiad",["-10, 11"]="Gnisis",["10, 14"]="Tel Vos",["-10, 15"]="Ashalmawia",["10, 1"]="Uvirith's Grave",["10, -3"]="Nchuleftingth",["-10, 9"]="Berandas",["-11, 10"]="Gnisis",["-11, 11"]="Gnisis",["11, 14"]="Vos",["-11, 15"]="Ald Velothi",["11, 16"]="Ahemmusa Camp",["11, 20"]="Ald Daedroth",["1, -13"]="Ebonheart",["11, -5"]="Mount Kand",["-1, 18"]="Valenvaryon",["-11, 9"]="Koal Cave Entrance",["12, -10"]="Zaintiraris",["12, 13"]="Vos",["1, 21"]="Sanctus Shrine",["12, 4"]="Yansirramus",["12, -8"]="Molag Mar",["13, 14"]="Tel Mora",["13, -1"]="Erabenimsun Camp",["13, -8"]="Molag Mar",["-1, -3"]="Moonmoth Legion Fort",["14, -13"]="Tel Branora",["14, -4"]="Mount Assarnibibi",["15, -13"]="Tel Branora",["15, 1"]="Tel Fyr",["15, 5"]="Tel Aruhn",["1, -5"]="Fields of Kummu",["-17, 24"]="Solstheim Gyldenhul Barrow Entrance",["17, 4"]="Sadrith Mora",["17, 5"]="Sadrith Mora",["17, -6"]="Nchurdamz",["18, 3"]="Wolverine Hall",["18, 4"]="Sadrith Mora",["-19, 23"]="Thirsk",["19, -4"]="Holamayan",["-20, 23"]="Solstheim Lake Fjalding",["-20, 25"]="Skaal Village",["-20, 26"]="Skaal Village",["-2, -10"]="Seyda Neen",["2, -10"]="Vivec",["2, -11"]="Vivec Hlaalu",["-21, 23"]="Solstheim Lake Fjalding",["2, -13"]="Ebonheart",["-2, 15"]="Falasmaryon",["-22, 16"]="Fort Frostmoth",["-22, 17"]="Fort Frostmoth",["-2, -2"]="Balmora",["-2, -2"]="Balmora",["-2, 2"]="Caldera",["-23, 20"]="Solstheim Brodir Grove",["-23, 23"]="Solstheim Altar of Thrond",["-24, 19"]="Raven Rock",["-24, 25"]="Solstheim Hrothmund's Bane",["-24, 26"]="Solstheim Castle Karstaag",["2, 4"]="Ghostgate",["-25, 19"]="Raven Rock",["-25, 23"]="Solstheim Hvitkald Peak",["-25, 26"]="Solstheim Mortrag Glacier",["-25, 27"]="Solstheim Mortrag Glacier",["-2, 5"]="Buckmoth Legion Fort",["-26, 26"]="Solstheim Mortrag Glacier",["-26, 27"]="Solstheim Mortrag Glacier",["-2, 6"]="Ald-ruhn",["2, -6"]="Arvel Plantation",["-27, 22"]="Solstheim Thormoor's Watch",["-2, 7"]="Ald-ruhn",["2, -7"]="Dren Plantation",["2, 8"]="Dagoth Ur",["-2, -9"]="Seyda Neen",["3, -10"]="Vivec Foreign Quarter",["3, -11"]="Vivec Redoran",["-3, 12"]="Maar Gan",["3, -12"]="Vivec St. Delyn",["3, -13"]="Vivec Temple",["3, -14"]="Vivec Temple",["-3, -2"]="Balmora",["-3, -3"]="Balmora",["-3, 6"]="Ald-ruhn",["3, 7"]="Odrosal",["3, -9"]="Vivec",["4, -10"]="Vivec Foreign Quarter",["4, -11"]="Vivec Arena",["4, -12"]="Vivec St. Olms",["4, -13"]="Vivec Temple",["4, -14"]="Vivec Temple",["-4, 18"]="Urshilaku Camp",["-4, 21"]="Ald Redaynia",["-4, -2"]="Balmora",["4, -3"]="Marandus",["4, 9"]="Tureynulal",["5, -10"]="Vivec",["5, -11"]="Vivec Telvanni",["5, 15"]="Zergonipal",["-5, 18"]="Ashurnabitashpi",["-5, -5"]="Odai Plateau",["-5, 9"]="Bal Isra",["6, 18"]="Rotheran",["-6, -1"]="Hlormaren",["6, 21"]="Mzuleft Ruin",["6, -5"]="Bal Ur",["-6, -5"]="Hla Oad",["6, -6"]="Suran",["6, -7"]="Suran",["6, -9"]="Ald Sotha",["7, 22"]="Dagon Fel",["-7, -4"]="Ashurnibibi",["8, -10"]="Mzahnch Ruin",["8, -12"]="Bal Fell",["8, 12"]="Nchuleft Ruin",["-8, 3"]="Gnaar Mok",["9, 10"]="Zainab Camp",["9, -12"]="Bal Fell",["-9, 16"]="Khuul",["-9, 17"]="Khuul",["-9, 4"]="Khartag Point",["-9, 5"]="Andasreth",["9, 6"]="Falensarano",["9, -7"]="Telasero"}

local cityes = {["-2, -9"]="Seyda Neen",["4, -10"]="Vivec Foreign Quarter",["5, -11"]="Vivec Telvanni",["4, -11"]="Vivec Arena",["4, -12"]="Vivec St. Olms",["3, -12"]="Vivec St. Delyn",["4, -11"]="Vivec Arena",["4, -13"]="Vivec Temple",["3, -11"]="Vivec Redoran",["2, -11"]="Vivec Hlaalu",["3, -13"]="Vivec Temple",["-20, 25"]="Skaal Village",["-19, 23"]="Thirsk",["7, 22"]="Dagon Fel",["13, 14"]="Tel Mora",["11, 14"]="Vos",["15, 5"]="Tel Aruhn",["17, 4"]="Sadrith Mora",["18, 3"]="Wolverine Hall",["15, 1"]="Tel Fyr",["12, -8"]="Molag Mar",["14, -13"]="Tel Branora",["6, -6"]="Suran",["2, -13"]="Ebonheart",["0, -7"]="Pelagiad",["-1, -3"]="Moonmoth Legion Fort",["-4, -2"]="Balmora",["-6, -5"]="Hla Oad",["-8, 3"]="Gnaar Mok",["-11, 11"]="Gnisis",["-11, 15"]="Ald Velothi",["-9, 17"]="Khuul",["-2, 7"]="Ald-ruhn",["-2, 5"]="Buckmoth Legion Fort",["-3, 12"]="Maar Gan",["-2, 2"]="Caldera"}

function cells.size()
	return 8192
end

function cells.find_actor(cell_description, refId)
    local cell_data = LoadedCells[cell_description].data
    local find = false
    
    for i, k in pairs(cell_data.packets.actorList) do
        if cell_data.objectData[k].refId == refId then
            find = true
        end
    end
    
    return find
end

function cells.is_loaded(cell_description)
	return (LoadedCells[cell_description] ~= nil) and true or false
end

function cells.file_exists(cell_description)
	local home = tes3mp.GetDataPath() .. "/cell/"
	local f = io.open(home .. cell_description .. ".json", "r")
	
	if f ~= nil then
		io.close(f)
		return true
	end
	
	return false
end

function cells.center(cell_description)
	local cx, cy = cell_description:match(patterns.exteriorCell)
	local x, y = 0, 0
	
	if tonumber(cx) < 0 then
		cx = cx * -1
		x = (cx * 8192) - (8192 / 2)
		x = x * -1
	else
		x = (cx * 8192) - (8192 / 2)
	end
	
	if tonumber(cy) < 0 then
		cy = cy * -1
		y = (cy * 8192) - (8192 / 2)
		y = y * -1
	else
		y = (cy * 8192) - (8192 / 2)
	end
	
	return x, y
end

function cells.insert_actor(cell_description, refid, location, scale)	
	LoadedCells[cell_description] = Cell(cell_description)
	
	local mpnum = WorldInstance:GetCurrentMpNum()
	WorldInstance:SetCurrentMpNum(mpnum)

	local data = {}
	data.refId = refid
	data.location = location
	data.scale = scale or 1
	
	local num = "0-" .. tostring(mpnum)
	local spawn = LoadedCells[cell_description].data.packets.spawn
	local scale = LoadedCells[cell_description].data.packets.scale
	
	spawn[#spawn+1] = num
	scale[#scale+1] = num
	
	LoadedCells[cell_description].data.objectData[num] = data
	
	jsonInterface.save("cell/" .. cell_description .. ".json", LoadedCells[cell_description].data, config.cellKeyOrder)
end

function cells:insert_object(cell_description, refid, posZ, scale)
	local x, y = self.center(cell_description)
	
	LoadedCells[cell_description] = Cell(cell_description)
	
	local mpnum = WorldInstance:GetCurrentMpNum()
	WorldInstance:SetCurrentMpNum(mpnum)

	local data = {}
	data.refId = refid
	data.location = {}
	data.location.posX = x
	data.location.posY = y
	data.location.posZ = posZ
	data.location.rotX = 0
	data.location.rotY = 0
	data.location.rotZ = 0
	data.scale = scale or 1
	
	local num = "0-" .. tostring(mpnum)
	local place = LoadedCells[cell_description].data.packets.place
	local scale = LoadedCells[cell_description].data.packets.scale
	
	place[#place+1] = num
	scale[#scale+1] = num
	
	LoadedCells[cell_description].data.objectData[num] = data
	
	jsonInterface.save("cell/" .. cell_description .. ".json", LoadedCells[cell_description].data, config.cellKeyOrder)
end

function cells.find_nearest_city_from_point(pid, current_cell)
	local cx, cy = current_cell:match(patterns.exteriorCell)
	local point = {
		description = "",
		distance = 999
	}

	for cell, description in pairs(cityes) do
		local x, y = cell:match(patterns.exteriorCell)
		local cdistance = math.sqrt((x - cx)^2 + (y - cy)^2)
		
		if point.distance > cdistance then
			point.description = tostring(x) .. ", " .. tostring(y)
			point.distance = cdistance
		end
	end

	return point
end

function cells.find_nearest_city(pid)
	local current = tes3mp.GetCell(pid)
	return cells.find_nearest_city_from_point(pid, current)
end

function cells.get(x, y)
	local cell = data[tostring(x) .. ", " .. tostring(y)]
	return cell or tostring(x) .. ", " .. tostring(y)
end

function cells.by_string(cell_description)
	local cell = data[cell_description]
	return cell or cell_description
end

function cells.is_region(cell)
    if LoadedCells[cell] == nil then
        return false
    end
    
	if data[cell] == nil and
		LoadedCells[cell].isExterior then
		return true
	end
	
	return false
end

function cells.current_region(pid)
	local region = tes3mp.GetRegion(pid)
	
	if region == "" then
		region = "Wastelands"
	end
	
	return region, tes3mp.GetCell(pid)
end

function cells:current_cell(pid)
	local cell = tes3mp.GetCell(pid)
	
	if LoadedCells[cell] ~= nil and LoadedCells[cell].isExterior then
		return data[cell]
	end
	
	return cell
end

return cells
