local Methods = {}

function Methods.DeleteCell(cell)
	if LoadedCells[cell] ~= nil then
		LoadedCells[cell] = nil
	end
	
	os.remove(tes3mp.GetDataPath() .. "/cell/" .. cell .. ".json")
end

return Methods
