logicHandler = require("logicHandler")

Methods = {}

function Methods.ForceGreet(pid, actor)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format("\"%s\" -> forcegreeting", actor), false)
end

function Methods.ResetActors(pid)
	logicHandler.RunConsoleCommandOnPlayer(pid, "ra", false)
end

function Methods.ResurrectActor(pid, actor)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format("\"%s\" -> resurrect", actor), false)
end

return Methods
