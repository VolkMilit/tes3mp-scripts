Methods = {}

local translate = {
    ["en"] = {
        seconds = "seconds",              
        minutes = "minutes",
        hours = "hours",
        weeks = "weeks",
        months = "months",
        years = "years",
        milleniums = "milleniums",
        the_universe_died = "the universe died"
    },
    
    ["ru"] = {
        seconds = "секунд",              
        minutes = "минут",
        hours = "часов",
        weeks = "недель",
        months = "месяцев",
        years = "лет",
        milleniums = "веков",
        the_universe_died = "вселенная умрет"
    }
}

function Methods.return_time_string(t, lang)   
    if lang == nil or translate[lang] == nil then
        lang = "en"
    end
    
    local tr = translate[lang]
    
    if t < 60 then
        return tr.seconds
    elseif t > 60 and t < 3600 then
        return tr.minutes
    elseif t > 3600 and t < 86400 then
        return tr.hours
    elseif t > 604800 and t < 2592000 then
        return tr.weeks
    elseif t > 2592000 and t < 5184000 then
        return tr.months
    elseif t > 31536000 then
        return tr.years
    elseif t > 3153600000 then
        return tr.milleniums
    else
        -- In the year one million and a half 
        -- humankind is enslaved by giraffe.
        -- Man must pay for all his misdeeds
        -- when the treetops are stripped of their leaves.
        return tr.the_universe_died
    end
end

function Methods.seconds_to(time_string)
    local current = os.date('*t')
    local sec = os.time({year=current.year, month=current.month, day=current.day, hour=19, minute=00}) - os.time(os.date('*t'))
    return sec
end

return Methods
