local Methods = {}

function Methods.delete(pid, cell, uniqueIndex)
	if LoadedCells[cell] == nil then
		tes3mp.LogMessage(enumerations.log.ERROR, "No cell named " .. cell .. " found.")
		return
	end

	local delete = LoadedCells[cell].data.packets.delete
	local obj = tableHelper.containsValue(delete, uniqueIndex)
	
	if obj == false then
		logicHandler.DeleteObject(pid, cell, uniqueIndex, true)
        table.insert(delete, uniqueIndex)
        
        if uniqueIndex:split("-")[1] == "0" then
            tableHelper.removeValue(LoadedCells[cell].data.packets.place, uniqueIndex)
            LoadedCells[cell].data.objectData[uniqueIndex] = nil
        end
	end
end

function Methods.is_container(cell, uniqueIndex)
    local locks = LoadedCells[cell].data.packets.container
    return tableHelper.containsValue(locks, uniqueIndex)
end

function Methods.set_lock(pid, cell, objects)
	if LoadedCells[cell] == nil then
		tes3mp.LogMessage(enumerations.log.ERROR, "No cell named " .. cell .. " found.")
		return
	end

	local locks = LoadedCells[cell].data.packets.lock
	for i, object in pairs(objects) do
		local obj = tableHelper.containsValue(locks, object)
	
		if obj == false then
			LoadedCells[cell].data.objectData[object.id] = {}
			LoadedCells[cell].data.objectData[object.id].refId = object.refId
			LoadedCells[cell].data.objectData[object.id].lockLevel = object.level
			table.insert(locks, object.id)
		end
	end
	
	if #locks > 0 then
		LoadedCells[cell]:LoadObjectsLocked(pid, LoadedCells[cell].data.objectData, locks)
	end
end

function Methods.move(pid, cell, refId, uniqueIndex, pos)
    if LoadedCells[cell]:ContainsObject(uniqueIndex) then
        -- At least one coordinate must be NOT nil        
        if pos.X == nil and pos.Y == nil and pos.Z == nil then
            return false
        end
        
        local location = LoadedCells[cell].data.objectData[uniqueIndex].location
        
        if pos.X ~= nil then
            location.posX = pos.X
        end
        
        if pos.Y ~= nil then
            location.posY = pos.Y
        end
        
        if pos.Z ~= nil then
            location.posZ = pos.Z
        end
        
        Methods.delete(pid, cell, uniqueIndex)
        return logicHandler.CreateObjectAtLocation(cell, location, refId, "place")
        
        -- We're successfully move the object! Yay!
        -- Louder!
        -- *deep breeze* yaaaaay!~
        -- Argh!
        -- Too loud?
    end
    
    -- Can't move the object
    return false
end

function Methods.rotate(pid, cell, refId, uniqueIndex, rot)
    if LoadedCells[cell]:ContainsObject(uniqueIndex) then
        -- At least one coordinate must be NOT nil
        if rot.X == nil and rot.Z == nil then
            return false
        end
        
        local location = LoadedCells[cell].data.objectData[uniqueIndex].location
        
        if rot.X ~= nil then
            location.rotX = rot.X
        end
        
        if rot.Z ~= nil then
            location.rotZ = rot.Z
        end
        
        Methods.delete(pid, cell, uniqueIndex)
        return logicHandler.CreateObjectAtLocation(cell, location, refId, "place")
    end
    
    -- Can't move the object
    return false
end

return Methods
