# Raise

Classes for easily interact with tes3mp api and scripts.

### actors.lua

| Method                     | Description                                          |
|----------------------------|------------------------------------------------------|
| ForceGreet(pid, actor)     | Similar to console command                           |
| ResetActors(pid)           | Similar to console command `ra`                      |
| ResurrectActor(pid, actor) | Similar to console command `"actor name"->ressurect` |

### classes.lua

Return table of all player classes.

```lua
local classes = require("raise.classes")
print(classes["agent"].name) -- print name
classes["agent"].attributes -- return array of attributes
print(classes["agent"].specialisation) -- print specialisation
classes["agent"].majorSkills -- return major skills
classes["agent"].minorSkills -- return minor skills
```

### console.lua

| Method                       | Description                |
|------------------------------|----------------------------|
| Set(pid, func, field, value) | Similar to console command |
| SetGlobal(pid, field, value) | Similar to console command |
| StartScript(pid, script)     | Similar to console command |

### diseases.lua

This class allows you to control diseases, somthing that tes3mp do bad.

| Method                 | Description                                                                         |
|------------------------|-------------------------------------------------------------------------------------|
| GetCommonDisease(pid)  | Return true if player is infected by common disease (ony works with vanila disease) |
| CureCommonDisease(pid) | Cure player from all known common diseases                                          |
| GetBlightDisease(pid)  | Return true if player is infected by blight disease (ony works with vanila disease) |
| CureBlightDisease(pid) | Cure player from all known common diseases                                          |
| GetPlayerPoison(pid)   | Return true, if player is poisoned                                                  |

### inventory.lua

Inventory helper class. Unlike `inventoryHelper`, this class will also send package to player(s).

| Method                                                      | Description                                      |
|-------------------------------------------------------------|--------------------------------------------------|
| AddItem(pid, refId, count, enchantmentCharge, charge, soul) | Add item to player inventory                     |
| RemoveItem(pid, refId, count)                               | Remove item from player inventory                |
| AddGold(pid, count)                                         | Add gold to player inventory and play sound      |
| RemoveGold(pid, amount)                                     | Remove gold from player inventory and play sound |
| GetGold(pid)                                                | Get amount of gold in player inventory           |

### player.lua

| Method                           | Description                                                                     |
|----------------------------------|---------------------------------------------------------------------------------|
| COC(pid, cell, pos, rot)         | Teleport player in coordinates, pos and rot must be tables with X and Y members |
| RemoveSpellEffects(pid, spellId) | Similar to console command                                                      |
| RemoveSpell(pid, spellId)        | Similar to console command                                                      |
| Controlls(pid, enable)           | Enable or disable player controlls                                              |
| PayFine(pid)                     | Player will force pay fine                                                      |
| Resurrect(pid)                   | Similar to console command                                                      |
| GetQuestIndexes(pid, questid)    | Get current state of one quest, table of states will be returned                |

### server.lua

| Method                        | Description                                                      |
|-------------------------------|------------------------------------------------------------------|
| DeleteCell(cell)              | Will delete cell and clear it, if it was loaded                  |

### sfx.lua

| Method                        | Description                                                      |
|-------------------------------|------------------------------------------------------------------|
| StreamMusic(pid, file)        | Similar to console command                                       |
| FadeIn(pid, sec)              | Similar to console command                                       |
| FadeOut(pid, sec)             | Similar to console command                                       |
| TM(pid)                       | Similar to console command                                       |
| PlayBIK(pid, file)            | Similar to console command                                       |

### world.lua

| Method                                     | Description                                                                                    |
|--------------------------------------------|------------------------------------------------------------------------------------------------|
| SetObjectState(pid, cell, uniqueIndex, state) | Set state of the object in world and send package                                           |
| Cast(pid, spellId, objectId, targetId)     | Cast spell from actor or object to target (note: this was broken in OpenMW 0.44 (tes3mp 0.7))) |
