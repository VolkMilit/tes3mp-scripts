tableHelper = require("tableHelper")

local Methods = {}

function Methods.GetCommonDisease(pid)
	local deseases = {
		"helljoint",
		"ataxia",
		"swamp fever",
		"brown rot",
		"witchwither",
		"vampire blood aundae",
		"vampire blood berne",
		"vampire blood quarra",
		"werewolf blood",
		"rotbone",
		"crimson_plague",
		"yellow tick",
		"greenspore",
		"serpiginous dementia",
		"droops",
		"wither",
		"rockjoint",
		"chills",
		"dampworm",
		"witbane",
		"collywobbles",
		"rattles",
		"rust chancre"
	}
	
	if tableHelper.containsValue(Players[pid].data.spellbook, deseases) then
		return true
	end
	
	return false
end

function Methods.CureCommonDisease(pid)
	local deseases = {
		"helljoint",
		"ataxia",
		"swamp fever",
		"brown rot",
		"witchwither",
		"vampire blood aundae",
		"vampire blood berne",
		"vampire blood quarra",
		"werewolf blood",
		"rotbone",
		"crimson_plague",
		"yellow tick",
		"greenspore",
		"serpiginous dementia",
		"droops",
		"wither",
		"rockjoint",
		"chills",
		"dampworm",
		"witbane",
		"collywobbles",
		"rattles",
		"rust chancre"
	}
	
	for ind, item in pairs(deseases) do
		table.removeValue(item)
	end
end

function Methods.GetBlightDisease(pid)
	local deseases = {
		"ash woe blight",
		"ash-chancre",
		"black-heart blight",
		"chanthrax blight",
		"corprus"
	}
	
	if tableHelper.containsValue(Players[pid].data.spellbook, deseases) then
		return true
	end
	
	return false
end

function Methods.CureBlightDisease(pid)
	local deseases = {
		"ash woe blight",
		"ash-chancre",
		"black-heart blight",
		"chanthrax blight",
		"corprus"
	}
	
	for ind, item in pairs(deseases) do
		table.removeValue(item)
	end
end

function Methods.GetPlayerPoison(pid)
	for ind, item in pairs(Players[pid].data.spellbook) do
		if item ~= "resist poison" and item ~= "resist poison_75" and item ~= "dire weakness to poison" then
			if string.match(item, "poison") ~= nil then
				return true
			end
		end
	end
	
	return false
end

return Methods
