inventoryHelper = require("inventoryHelper")
tableHelper = require("tableHelper")
require("enumerations")

local Methods = {}

function Methods.HasItem(pid, refid)
	local invInd = inventoryHelper.getItemIndex(Players[pid].data.inventory, refId, -1, -1, "")
	
	if invInd ~= nil then
		return Players[pid].data.inventory[invInd].count
	end
	
	return 0
end

function Methods.AddItem(pid, refId, count, enchantmentCharge, charge, soul)
	if enchantmentCharge == nil then
		enchantmentCharge = -1
	end
	
	if charge == nil then
		charge = -1
	end
	
	if soul == nil then
		soul = ""
	end

	local item = {
		refId = refId,
		count = count,
		enchantmentCharge = enchantmentCharge,
		charge = charge,
		soul = soul
	}

	inventoryHelper.addItem(Players[pid].data.inventory, refId, count, enchantmentCharge, charge, soul)
	Players[pid]:LoadItemChanges({item}, enumerations.inventory.ADD)
end

function Methods.RemoveItem(pid, refId, count)
	local eqInd = tableHelper.getIndexByNestedKeyValue(Players[pid].data.equipment, "refId", refId, true)
	
	if eqInd ~= nil then
		Players[pid].data.equipment[eqInd] = nil
	end
	
	local invInd = inventoryHelper.getItemIndex(Players[pid].data.inventory, refId, -1, -1, "")
	
	if invInd ~= nil then
		if tonumber(count) ~= nil then
			Players[pid].data.inventory[invInd].count = Players[pid].data.inventory[invInd].count - count
		elseif count == "all" then
			Players[pid].data.inventory[invInd] = nil
			tableHelper.cleanNils(Players[pid].data.inventory)
		end
		
		Players[pid]:LoadInventory()
		Players[pid]:LoadEquipment()
	end
end

function Methods.AddGold(pid, count)    
    if type(pid) == "string" then
        pid = logicHandler.GetPlayerByName(pid).pid
    end
    
    local player = Players[pid]
    
    if player ~= nil and player:IsLoggedIn() then
        inventoryHelper.addItem(player.data.inventory, "gold_001", count, -1, -1, "")
        player:QuicksaveToDrive()
        local itemref = {refId = "gold_001", count = count, charge = -1, soul = -1}
        player:LoadItemChanges({itemref}, enumerations.inventory.ADD)
    else
        -- pid here are player name
        player = jsonInteraface.load("player/" .. pid .. ".json")
        inventoryHelper.addItem(player.inventory, "gold_001", count, -1, -1, "")
        jsonInteraface.save("player/" .. pid .. ".json", player)
    end
end

function Methods.RemoveGold(pid, amount)
    if type(pid) == "string" then
        pid = logicHandler.GetPlayerByName(pid).pid
    end
    
    local player = Players[pid]
    
    if player ~= nil and player:IsLoggedIn() then
        inventoryHelper.removeExactItem(player.data.inventory, "gold_001", amount, -1, -1, "")
        player:QuicksaveToDrive()
        local itemref = {refId = "gold_001", count = amount, charge = -1, soul = -1}
        player:LoadItemChanges({itemref}, enumerations.inventory.REMOVE)
    else
        -- pid here are player name
        player = jsonInteraface.load("player/" .. pid .. ".json")
        inventoryHelper.removeExactItem(player.data.inventory, "gold_001", amount, -1, -1, "")
        jsonInteraface.save("player/" .. pid .. ".json", player)
    end
end

function Methods.GetGold(pid)
	local index = inventoryHelper.getItemIndex(Players[pid].data.inventory, "gold_001", -1, -1, "")
	if index ~= nil then
		return Players[pid].data.inventory[index].count
	else
		return 0
	end
end

return Methods
