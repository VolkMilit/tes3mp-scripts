logicHandler = require("logicHandler")

local Methods = {}

function Methods.Set(pid, func, field, value)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format('set "%s".%s to %s', func, field, value), false)
end

function Methods.SetGlobal(pid, field, value)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format('set "%s" to %s', field, value), false)
end

function Methods.StartScript(pid, script)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format('startscript "%s"', script), false)
end

function Methods.stop_script(pid, script)
    logicHandler.RunConsoleCommandOnPlayer(pid, string.format('stopscript "%s"', script), false)
end

function Methods.run_in_cell(cell, cmds)
    for pid, _ in pairs(Players) do
        if tes3mp.GetCell(pid) == cell then
            for ind, cmd in pairs(cmds) do
                logicHandler.RunConsoleCommandOnPlayer(pid, cmd, false)
            end
        end
    end
end

return Methods
