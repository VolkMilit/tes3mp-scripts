local Methods = {}
local items = jsonInterface.load("custom/raise/items.json")

function Methods.object_i18n(refId)
    if items[refId] ~= nil then
        return items[refId].name
    else
        return refId
    end
end

function Methods.get_data(refId)
    return items[refId]
end

function Methods.is_item(refId)
    -- Base game item
    if items[refId] ~= nil then
        return true
    end
    
    local records = {"book", "miscellaneous", "armor", "clothing", "potion", "weapon"}

    -- Probably generated item?
    for _, record in pairs(records) do
        if RecordStores[record].data.permanentRecords[refId] ~= nil or
                RecordStores[record].data.generatedRecords ~= nil then
            return true
        end
    end
    
    -- Nope
    return false
end

return Methods
