local Methods = {}

function Methods.new(destanation)
	Menus[destanation] = {}
	Menus[destanation].text = ""
	Menus[destanation].buttons = {}
end

function Methods.insert_command(destanation, command, explanation)
	local text = Menus[destanation].text
	Menus[destanation].text = string.format("%s\n/%s\n%s", text, 
			color.Yellow .. command, color.White .. explanation)
end

function Methods.insert_button(destanation, caption, staff_rank, lead_to)
	local button = { 
		caption = caption,
		displayConditions = {
			menuHelper.conditions.requireStaffRank(staff_rank)
		},
		destinations = {
			menuHelper.destinations.setDefault(lead_to)
		}
	}
	
	-- We're insert to end, because Lua's arrays start at 1, but
	-- this particular function start count at 0
    if #Menus[destanation].buttons == 0 then
        table.insert(Menus[destanation].buttons, 1, button)
    else
        table.insert(Menus[destanation].buttons, #Menus[destanation].buttons, button)
    end
end

return Methods
