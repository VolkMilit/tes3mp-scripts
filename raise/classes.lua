return {

	["acrobat"] = {
		name = "Акробат",
		attributes = {"Agility", "Endurance"},
		specialisation = "Stealth",
		majorSkills = {"Acrobatics", "Athletics", "Marksman", "Sneak", "Unarmored"},
		minorSkills = {"Speechcraft", "Alteration", "Spear", "Handtohand", "Lightarmor"}
	},

	["agent"] = {
		name = "Шпион",
		attributes = {"Personality", "Agility"},
		specialisation = "Stealth",
		majorSkills = {"Speechcraft", "Sneak", "Acrobatics", "Lightarmor", "Shortblade"},
		minorSkills = {"Mercantile", "Conjuration", "Block", "Unarmored", "Illusion"}
	},

	["archer"] = {
		name = "Лучник",
		attributes = {"Agility", "Strength"},
		specialisation = "Combat",
		majorSkills = {"Marksman", "Longblade", "Block", "Athletics", "Lightarmor"},
		minorSkills = {"Unarmored", "Spear", "Restoration", "Sneak", "Mediumarmor"}
	},

	["assassin"] = {
		name = "Ассассин",
		attributes = {"Speed", "Intelligence"},
		specialisation = "Stealth",
		majorSkills = {"ShortBlade", "Sneak", "Marksman", "LightArmor", "Acrobatics"},
		minorSkills = {"Security", "Longblade", "Alchemy", "Block", "Athletics"}
	},

	["barbarian"] = {
		name = "Варвар",
		attributes = {"Strength", "Speed"},
		specialisation = "Combat",
		majorSkills = {"Axe", "Mediumarmor", "Bluntweapon", "Athletics", "Block"},
		minorSkills = {"Acrobatics", "Lightarmor", "Armorer", "Marksman", "Unarmored"}
	},

	["bard"] = {
		name = "Бард",
		attributes = {"Personality", "Intelligence"},
		specialisation = "Stealth",
		majorSkills = {"Speechcraft", "Alchemy", "Acrobatics", "Longblade", "Block"},
		minorSkills = {"Mercantile", "Illusion", "Mediumarmor", "Enchant", "Security"}
	},

	["battlemage"] = {
		name = "Боевой Маг",
		attributes = {"Intelligence", "Strength"},
		specialisation = "Magic",
		majorSkills = {"Alteration", "Destruction", "Conjuration", "Axe", "HeavyArmor"},
		minorSkills = {"Mysticism", "Longblade", "Marksman", "Enchant", "Alchemy"}
	},

	["crusader"] = {
		name = "Паладин",
		attributes = {"Agility", "Strength"},
		specialisation = "Combat",
		majorSkills = {"Bluntweapon", "Longblade", "Destruction", "Heavyarmor", "Block"},
		minorSkills = {"Restoration", "Armorer", "Handtohand", "Mediumarmor", "Alchemy"}
	}, 

	["healer"] = {
		name = "Целитель",
		attributes = {"Willpower", "Personality"},
		specialisation = "Magic",
		majorSkills = {"Restoration", "Mysticism", "Alteration", "Handtohand", "Speechcraft"},
		minorSkills = {"Illusion", "Alchemy", "Unarmored", "Lightarmor", "Bluntweapon"}
	},

	["knight"] = {
		name = "Рыцарь",
		attributes = {"Strength", "Personality"},
		specialisation = "Combat",
		majorSkills = {"Longblade", "Axe", "Speechcraft", "Heavyarmor", "Block"},
		minorSkills = {"Restoration", "Mercantile", "Mediumarmor", "Enchant", "Armorer"}
	},

	["mage"] = {
		name = "Маг",
		attributes = {"Intelligence", "Willpower"},
		specialisation = "Magic",
		majorSkills = {"Mysticism", "Destruction", "Alteration", "Illusion", "Restoration"},
		minorSkills = {"Enchant", "Alchemy", "Unarmored", "ShortBlade", "Conjuration"}
	},

	["monk"] = {
		name = "Монах",
		attributes = {"Agility", "Willpower"},
		specialisation = "Stealth",
		majorSkills = {"Handtohand", "Unarmored", "Athletics", "Acrobatics", "Sneak"},
		minorSkills = {"Block", "Marksman", "Lightarmor", "Restoration", "Bluntweapon"}
	},

	["nightblade"] = {
		name = "Меч Ночи",
		attributes = {"Willpower", "Speed"},
		specialisation = "Magic",
		majorSkills = {"Mysticism", "Illusion", "Alteration", "Sneak", "Shortblade"},
		minorSkills = {"Lightarmor", "Unarmored", "Destruction", "Marksman", "Security"}
	},

	["pilgrim"] = {
		name = "Пилигрим",
		attributes = {"Personality", "Endurance"},
		specialisation = "Stealth",
		majorSkills = {"Speechcraft", "Mercantile", "Marksman", "Restoration", "Mediumarmor"},
		minorSkills = {"Illusion", "Handtohand", "Shortblade", "Block", "Alchemy"}
	},

	["rouge"] = {
		name = "Жулик",
		attributes = {"Speed", "Personality"},
		specialisation = "Combat",
		majorSkills = {"Shortblade", "Mercantile", "Axe", "Lightarmor", "Handtohand"},
		minorSkills = {"Block", "Mediumarmor", "Speechcraft", "Athletics", "Longblade"}
	},

	["scout"] = {
		name = "Разветчик",
		attributes = {"Speed", "Endurance"},
		specialisation = "Combat",
		majorSkills = {"Sneak", "Longblade", "Mediumarmor", "Athletics", "Block"},
		minorSkills = {"Marksman", "Alchemy", "Alteration", "Lightarmor", "Unarmored"}
	},

	["sorcerer"] = {
		name = "Чародей",
		attributes = {"Intelligence", "Endurance"},
		specialisation = "Magic",
		majorSkills = {"Enchant", "Conjuration", "Mysticism", "Destruction", "Alteration"},
		minorSkills = {"Illusion", "Mediumarmor", "Heavyarmor", "Marksman", "Shortblade"}
	},

	["spellsword"] = {
		name = "Воин Слова",
		attributes = {"Willpower", "Endurance"},
		specialisation = "Magic",
		majorSkills = {"Block", "Restoration", "Longblade", "Destruction", "Alteration"},
		minorSkills = {"Bluntweapon", "Enchant", "Alchemy", "Mediumarmor", "Axe"}
	},

	["thief"] = {
		name = "Вор",
		attributes = {"Speed", "Agility"},
		specialisation = "Magic",
		majorSkills = {"Security", "Sneak", "Acrobatics", "Lightarmor", "Shortblade"},
		minorSkills = {"Marksman", "Speechcraft", "Handtohand", "Mercantile", "Athletics"}
	},

	["warrior"] = {
		name = "Воин",
		attributes = {"Strength", "Endurance"},
		specialisation = "Combat",
		majorSkills = {"Longblade", "Mediumarmor", "Heavyarmor", "Athletics", "Block"},
		minorSkills = {"Armorer", "Spear", "Marksman", "Axe", "Bluntweapon"}
	},

	["witchhunter"] = {
		name = "Инквизитор",
		attributes = {"Intelligence", "Agility"},
		specialisation = "Magic",
		majorSkills = {"Conjuration", "Enchant", "Alchemy", "Lightarmor", "Marksman"},
		minorSkills = {"Unarmored", "Block", "Bluntweapon", "Sneak", "Mysticism"}
	}

}
