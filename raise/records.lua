local function create_base_record(pid, record_type, params, permanent)
	local record_store = RecordStores[record_type]
	local id = record_store:GenerateRecordId()
	
    if permanent then
        record_store.data.generatedRecords[id] = params

        -- This record will be sent to everyone on the server below, so track it
        -- as having already been received by players
        for _, player in pairs(Players) do
            if not tableHelper.containsValue(Players[pid].generatedRecordsReceived, id) then
                table.insert(player.generatedRecordsReceived, id)
            end
        end

	else
		record_store.data.permanentRecords[id] = params
    end

    record_store:QuicksaveToDrive()

    tes3mp.ClearRecords()
    tes3mp.SetRecordType(enumerations.recordType[string.upper(record_type)])
    
    return id
end

local data = jsonInterface.load("custom/raise/data.json")

local Methods = {}

function Methods.create_book(pid, params, permanent)
	if params.skillId == nil then
		params.skillId = -1
	end
	
	local id = create_base_record(pid, "book", params, permanent or false)
	packetBuilder.AddBookRecord(id, params)
	
	return id
end

function Methods.create_misc(pid, params, permanent)
	local id = create_base_record(pid, "miscellaneous", params, permanent or false)
	packetBuilder.AddMiscellaneousRecord(id, params)

	return id
end

function Methods.create_armor(pid, params, permanent)
	local id = create_base_record(pid, "armor", params, permanent or false)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_clothing(pid, params, permanent)
	local id = create_base_record(pid, "clothing", params, permanent or false)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_creature(pid, params)
	local id = create_base_record(pid, "creature", params, false)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_enchantment(pid, params)
	local id = create_base_record(pid, "enchantment", params, true)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_potion(pid, params, permanent)
	local id = create_base_record(pid, "potion", params, permanent or false)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_spell(pid, params)
	local id = create_base_record(pid, "spell", params, true)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_weapon(pid, params, permanent)
	local id = create_base_record(pid, "weapon", params, permanent or false)
	packetBuilder.AddMiscellaneousRecord(id, params)
	
	return id
end

function Methods.create_npc(pid, params)
	if params.gender == "male" then
		params.gender = 1
	elseif params.gender == "female" then
		params.gender = 0
	else
		params.gender = 1
	end
	
	if params.aiFight == true then
		params.aiFight = 1
	else
		params.aiFight = 0
	end
	
	if params.autoCalc == nil then
		params.autoCalc = 1
	end
	
	local id = create_base_record(pid, "npc", params, false)
	packetBuilder.AddNpcRecord(id, params)
	
	return id
end

function Methods.send_records(pid, to_all)
	tes3mp.SendRecordDynamic(pid, true, to_all or false)
end

function Methods.create_alias(id, alias)   
    data.links[alias] = id
    jsonInterface.quicksave("custom/raise/data.json", data)
end

function Methods.get_record_by_alias(alias)
    return data.links[alias]
end

function Methods.delete_alias(alias)
    data.links[alias] = nil
    jsonInterface.quicksave("custom/raise/data.json", data)
end

function Methods.delete_record(id, record_type)
	local record_store = RecordStores[record_type].data
	local refId = "$custom_" .. record_type .. "_" .. id
	local permament = record_store.permanentRecords[refId]
	local generated = record_store.generatedRecords[refId]
	local players_has_record = record_store.recordLinks[refId].players
	
	if record_store == nil then
		return false
	end
	
	if permanent == nil then
		if generated == nil then
			return false
		else
			record_store.generatedRecords[refId] = nil
		end
	else
		record_store.permanentRecords[refId] = nil
	end
	
	-- todo: check unloged players for record
	
	-- todo: check players inventory for record
	if players_has_record ~= nil then
		for pid, player in pairs(Players) do
			player.data.recordLinks[record_type][refId] = nil
		end
	end
	
	-- todo: check cells for record
		
	return true
end

return Methods
