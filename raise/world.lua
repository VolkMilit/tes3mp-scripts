local Methods = {}

function Methods.SetObjectState(pid, cell, uniqueIndex, state)
	LoadedCells[cell].data.objectData[uniqueIndex].state = state

    tes3mp.ClearObjectList()
    tes3mp.SetObjectListPid(pid)
    tes3mp.SetObjectListCell(cell)
    packetBuilder.AddObjectState(uniqueIndex, LoadedCells[cell].data.objectData[uniqueIndex])
    tes3mp.SendObjectState()
end

function Methods.Cast(pid, spellId, objectId, targetId)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format("\"%s\" -> cast \"%s\" \"%s\"", objectId, spellId, targetId), false)
end

return Methods
