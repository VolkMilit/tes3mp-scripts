local Methods = {}

function Methods.magic_teleport(pid, cell, pos, rot)
    tes3mp.PlaySpeech(pid, "Fx\\magic\\conjH.wav")
    Methods.teleport(pid, cell, pos, rot)
end

function Methods.teleport(pid, cell, pos, rot)
    tes3mp.SetCell(pid, cell)
	tes3mp.SendCell(pid)
	
	if pos ~= nil then
		tes3mp.SetPos(pid, pos.posX, pos.posY, pos.posZ)
	end
	
	if rot ~= nil then
		tes3mp.SetRot(pid, rot.rotX, rot.rotZ)
	end
	
	if rot ~= nil or pos ~= nil then
		tes3mp.SendPos(pid)
	end
end

-- Depricated
function Methods.COC(pid, cell, pos, rot)
	Methods.teleport(pid, cell, pos, rot)
end

function Methods.SetBounty(pid, bounty)
	tes3mp.SetBounty(pid, bounty)
    tes3mp.SendBounty(pid)
	self:SaveBounty()
end

function Methods.RemoveSpellEffects(pid, spellId)
	logicHandler.RunConsoleCommandOnPlayer(pid, "player->removespelleffects \"" .. spellId .. "\"", false)
end

function Methods.RemoveSpell(pid, spellId)
	logicHandler.RunConsoleCommandOnPlayer(pid, "player->removespell \"" .. spellId .. "\"", false)
end

function Methods.Controlls(pid, enable)
	if enable == true then
		logicHandler.RunConsoleCommandOnPlayer(pid, "enableplayercontrols", false)
	else
		logicHandler.RunConsoleCommandOnPlayer(pid, "disableplayercontrols", false)
	end
end

function Methods.PayFine(pid)
	logicHandler.RunConsoleCommandOnPlayer(pid, "payfine", false)
end

function Methods.Resurrect(pid)
	tes3mp.Resurrect(pid, enumerations.resurrect.REGULAR)
end

function Methods.GetQuestIndexes(pid, questid)
	local indexes = {}

	for ind, quest in pairs(Players[pid].data.journal) do
		if quest ~= nil and quest.quest == questid then
			indexes[#indexes+1] = quest.index
		end
	end
	
	return indexes
end

return Methods
