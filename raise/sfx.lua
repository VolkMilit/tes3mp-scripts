logicHandler = require("logicHandler")

local Methods = {}

function Methods.StreamMusic(pid, file)
	logicHandler.RunConsoleCommandOnPlayer(pid, "streammusic \"" .. file .. "\"", false)
end

function Methods.FadeIn(pid, sec)
	logicHandler.RunConsoleCommandOnPlayer(pid, "fadein " .. sec, false)
end

function Methods.FadeOut(pid, sec)
	logicHandler.RunConsoleCommandOnPlayer(pid, "fadeout " .. sec, false)
end

function Methods.TM(pid)
	logicHandler.RunConsoleCommandOnPlayer(pid, "tm", false)
end

function Methods.PlayBIK(pid, file)
	logicHandler.RunConsoleCommandOnPlayer(pid, string.format("playbink \"%s.bik\" 1", file), false)
end

return Methods
